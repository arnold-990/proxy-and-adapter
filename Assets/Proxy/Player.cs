﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    GameInfo Session = new GameInfo();
    Proxy proxy = new Proxy();
    CheckLvl check = new CheckLvl();
    private void Start()
    {
        Session.CompleteLvl();
        proxy.CompleteLvl();
        check.CompleteLvl();
    }
}