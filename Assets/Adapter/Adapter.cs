﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Adapter : IWorker
{
    Builder builder1;
    Salary salary = new Salary(1000);
    public Adapter(Builder builder)
    {
        builder1 = builder;
    }
    public void Work()
    {
        builder1.Build();
    }
    public void PayMoney(int v)
    {
        builder1.GetSalary(new Salary(v));
    }
}