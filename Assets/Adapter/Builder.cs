﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Builder 
{
    public void Build()
    {
        Debug.Log("Строим Дом");
    }
    public void GetSalary(Salary salary)
    {
        Debug.Log("получено " + salary.Money + " рублей");
    }
}
public interface IWorker
{
    void Work();
    void PayMoney(int v);
}